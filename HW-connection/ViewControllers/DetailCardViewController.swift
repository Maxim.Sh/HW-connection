//
//  ViewController.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//

import UIKit

final class DetailCardViewController: UIViewController {

    @IBOutlet var detailLabel: UILabel!

    var card: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailLabel.text = card
    }
}

