//
//  ResourcesTableViewController.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 26.07.2023.
//

import UIKit

enum Categories: CaseIterable{
    case films
    case people
    case planets
    case species
    case starships
    case vehicles
    
    var title: String {
        switch self {
        case .films: return "Films"
        case .people: return "People"
        case .planets: return "Planets"
        case .species: return "Species"
        case .starships: return "Starships"
        case .vehicles: return "Vehicles"
        }
    }
    
    var url: String {
        switch self {
        case .films: return "https://swapi.dev/api/films/"
        case .people: return "https://swapi.dev/api/people/"
        case .planets: return "https://swapi.dev/api/planets/"
        case .species: return "https://swapi.dev/api/species/"
        case .starships: return "https://swapi.dev/api/starships/"
        case .vehicles: return "https://swapi.dev/api/vehicles/"
        }
    }
    
}

class CatalogViewController: UITableViewController {

    private let catalogs: [Categories] = Categories.allCases
    private let networkManager = NetworkManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 80
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        catalogs.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCatalog", for: indexPath)
        var content = cell.defaultContentConfiguration()
        
        content.text = catalogs[indexPath.row].title
        content.textProperties.alignment = .center
        
        cell.contentConfiguration = content
        
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let catalog = catalogs[indexPath.row]

        performSegue(withIdentifier: "showResorses", sender: catalog)

    }
    

    // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showResorses" {
             guard let resourceVC = segue.destination as? ResourceCollectionViewController else { return }
             guard let category = sender as? Categories else { return }
             resourceVC.category = category
            resourceVC.title = category.title
            resourceVC.fetchData()
        }
     }

}
