//
//  ResourceCollectionViewController.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 26.07.2023.
//

import UIKit

private let reuseIdentifier = "Cell"

class ResourceCollectionViewController: UICollectionViewController {
    
    // MARK: — Public Propereties
    var category: Categories?
    
    // MARK: — Private Propereties
    private let networkManager = NetworkManager.shared
    
    private var films: [Film] = []
    private var people: [Person] = []
    private var planets: [Planet] = []
    private var species: [Species] = []
    private var starships: [Starship] = []
    private var vehicles: [Vehicle] = []
    
    // MARK: — Override Methds
    //        override func viewDidLoad() {
    //            super.viewDidLoad()
    //        }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            
            guard let detailCardVC = segue.destination as? DetailCardViewController else { return }
            guard let indexPath = sender as? Int else { return }
            
            switch category {
            case .films: detailCardVC.card = films[indexPath].description
            case .people: detailCardVC.card = people[indexPath].description
            case .planets: detailCardVC.card = planets[indexPath].name
            case .species: detailCardVC.card = species[indexPath].description
            case .starships: detailCardVC.card = starships[indexPath].name
            case .vehicles: detailCardVC.card = vehicles[indexPath].name
            case .none:
                return
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch category {
        case .films: return films.count
        case .people: return people.count
        case .planets: return planets.count
        case .species: return species.count
        case .starships: return starships.count
        case .vehicles: return vehicles.count
        default: return 2
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: indexPath.item)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "cellResours",
            for: indexPath
        )
        
        guard let cell = cell as? ResourceViewCell else { return UICollectionViewCell() }

        switch category {
        case .films: cell.resourceLabel.text = films[indexPath.item].title
        case .people: cell.resourceLabel.text = people[indexPath.item].name
        case .planets: cell.resourceLabel.text = planets[indexPath.item].name
        case .species: cell.resourceLabel.text = species[indexPath.item].name
        case .starships: cell.resourceLabel.text = starships[indexPath.item].name
        case .vehicles: cell.resourceLabel.text = vehicles[indexPath.item].name
        default:
            cell.resourceLabel.text = ""
        }
        
        return cell
    }
}

// MARK: — UICollectionViewDelegateFlowLayout
extension ResourceCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: UIScreen.main.bounds.width / 2 - 24, height: 100)
    }
}

// MARK: - Networking
extension ResourceCollectionViewController {
    
    func fetchData() {
        
        guard let categoryRes = category else { return }
        guard let url = URL(string: categoryRes.url) else { return }
        
        switch categoryRes {
        case .films:
            networkManager.fetch(ResultFilms.self, from: url){ [weak self] result in
                switch result {
                case .success(let data):
                    self?.films = data.results
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        case .people:
            networkManager.fetch(ResultPeople.self, from: url){ [weak self] result in
                switch result {
                case .success(let data):
                    self?.people = data.results
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        case .planets:
            networkManager.fetch(ResultPlanets.self, from: url){ [weak self] result in
                switch result {
                case .success(let data):
                    self?.planets = data.results
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        case .species:
            networkManager.fetch(ResultSpecies.self, from: url){ [weak self] result in
                switch result {
                case .success(let data):
                    self?.species = data.results
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        case .starships:
            networkManager.fetch(ResultStarShips.self, from: url){ [weak self] result in
                switch result {
                case .success(let data):
                    self?.starships = data.results
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        case .vehicles:
            networkManager.fetch(ResultVehicles.self, from: url){ [weak self] result in
                switch result {
                case .success(let data):
                    self?.vehicles = data.results
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
        
    }
}
