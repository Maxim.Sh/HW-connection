//
//  DataManager.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 31.07.2023.
//

final class DataStore {
    static let shared = DataStore()
    
    var films: [Film] = []
    var people: [Person] = []
    var species: [Species] = []
    var planets: [Planet] = []
    var starships: [Starship] = []
    var cehicles: [Vehicle] = []
    
    private init() {}
}
