//
//  ResourceViewCell.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 27.07.2023.
//

import UIKit

final class ResourceViewCell: UICollectionViewCell {
    @IBOutlet var resourceLabel: UILabel!
}
