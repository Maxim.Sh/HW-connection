//
//  planetStarWars.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//

struct Film: Decodable {
    let title: String
    let episodeId:  Int
    let openingCrawl: String
    let director: String
    let producer: String
    let releaseDate: String
    let species: [String]
    let starships: [String]
    let vehicles: [String]
    let characters: [String]
    let planets: [String]
    let url: String

    var description: String {
        """
Название: \(title).
Номер эпизода; \(episodeId).
Дата выхода фильма: \(releaseDate).
Режиссер: \(director).
Продюсер(-ы): \(producer).


Начальные титры:
\(openingCrawl)
"""
    }
}

struct ResultFilms: Decodable {
    let count: Int
    let results: [Film]
}


