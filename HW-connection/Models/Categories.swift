//
//  Categories.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 31.07.2023.
//

struct Category: Decodable {
    let title: String
    let url:  String
}
