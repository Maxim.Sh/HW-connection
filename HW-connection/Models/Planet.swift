//
//  planetStarWars.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//

struct Planet: Decodable {
    let name: String
    let diameter: String
    let rotationPeriod: String
    let orbitalPeriod: String
    let gravity: String
    let population: String
    let climate: String
    let terrain: String
    let surfaceWater: String
    let residents: [String]
    let films: [String]
    let url: String
}

struct ResultPlanets: Decodable {
    let count: Int
    let results: [Planet]
}
