//
//  planetStarWars.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//

struct Vehicle: Decodable {
    let name: String
    let model: String
    let vehicleClass: String
    let manufacturer: String
    let length: String
    let costInCredits: String
    let crew: String
    let passengers: String
    let maxAtmospheringSpeed: String
    let cargoCapacity: String
    let consumables: String
    let films: [String]
    let pilots: [String]
    let url: String
}

struct ResultVehicles: Decodable {
    let count: Int
    let results: [Vehicle]
}
