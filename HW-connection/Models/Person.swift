//
//  planetStarWars.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//


struct Person: Decodable {
    let name: String
    let birthYear: String
    let eyeColor: String
    let hairColor: String
    let height: String
    let mass: String
    let skinColor: String
    let homeworld: String
    let films: [String]
    let species: [String]
    let starships: [String]
    let vehicles: [String]
    let url: String
    
    var description: String {
        """
Имя: \(name)
Дата рождения: \(birthYear)
Цвет глаз: \(eyeColor)
Цвет волос: \(hairColor)
Рост: \(height)
Вес: \(mass)
Цвет кожи: \(skinColor)
Родная планета: \(homeworld)
Тип: \(species.joined(separator: "\n         "))

Фильмы: \(films.joined(separator: "\n                 "))
Корабли: \(starships.joined(separator: "\n                 "))
Транспорт: \(vehicles.joined(separator: "\n                     "))
"""
    }
}


struct ResultPeople: Decodable {
    let count: Int
    let results: [Person]
}
