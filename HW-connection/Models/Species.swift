//
//  planetStarWars.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//

struct Species: Decodable {
    let name: String
    let classification: String
    let designation: String
    let averageHeight: String
    let averageLifespan: String
    let eyeColors: String
    let hairColors: String
    let skinColors: String
    let language: String
    let homeworld: String
    let people: [String]
    let films: [String]
    let url: String
    
    var description: String {
        """
Имя: \(name)
Классификация: \(classification)
Обозначение: \(designation)
Средний рост, см. \(averageHeight)
Средняя продолжительность жизни, год: \(averageLifespan)
Общие цвета глаз: \(eyeColors)
Общие цвета волос: \(hairColors)
Язык: \(language)
Родная планета: \(homeworld)

Фильмы: \(films.joined(separator: "\n                 "))
Корабли: \(people.joined(separator: "\n                 "))
"""
    }
}


struct ResultSpecies: Decodable {
    let count: Int
    let results: [Species]
}
