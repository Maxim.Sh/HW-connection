//
//  planetStarWars.swift
//  HW-connection
//
//  Created by Максим Шкрябин on 25.07.2023.
//

struct Starship: Decodable {
    let name: String
    let model: String
    let starshipClass: String
    let manufacturer: String
    let costInCredits: String
    let length: String
    let crew: String
    let passengers: String
    let maxAtmospheringSpeed: String
    let hyperdriveRating: String
    let MGLT: String
    let cargoCapacity: String
    let consumables: String
    let films: [String]
    let pilots: [String]
    let url: String
}

struct ResultStarShips: Decodable {
    let count: Int
    let results: [Starship]
}
